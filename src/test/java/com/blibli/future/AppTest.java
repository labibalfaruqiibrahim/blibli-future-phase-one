package com.blibli.future;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.blibli.future.page.*;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.openqa.selenium.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Unit test for simple App.
 */
public class AppTest {
    private static ExtentReports extent;
    private static int countScreenshot;
    private static String rootPath = System.getProperty("user.dir");
    /**
     * Rigorous Test :-)
     */

    //get app.properties
    private static String appConfigPath = rootPath + "\\app.properties";
    private static Properties appProps = new Properties();
    private static String browser;
    private String namaKotaHotelBenar;
    private String websiteBlibli;
    private String screenshotPath;
    private String namaPemesan;
    private String email;
    private String namaTamu;
    private String nomorTelepon;
    private int jumlahOrang, jumlahKamar, jumlahOrangdanKamarSama;
    private WebDriver webDriver;
    private WebDriverWait wait;

    @BeforeClass
    public static void beforeClass() throws IOException {
        countScreenshot = 0;

        //load properties
        appProps.load(new FileInputStream(appConfigPath));
        String locationextentReport = appProps.getProperty("locationextentReport");
        browser = appProps.getProperty("browser");
        // initialize the HtmlReporter
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(rootPath + locationextentReport);

        // initialize ExtentReports and attach the HtmlReporter
        extent = new ExtentReports();

        // attach HtmlReporter
        extent.attachReporter(htmlReporter);
        extent.setSystemInfo("Host Name", "SoftwareTestingMaterial");
        extent.setSystemInfo("Environment", "Automation Testing");
        extent.setSystemInfo("User Name", "Labib Alfaruqi Ibrahim");
        extent.setSystemInfo("Browser", browser);
        htmlReporter.setAppendExisting(true);
        htmlReporter.config().setDocumentTitle("QA Automation Report");
        htmlReporter.config().setReportName("Blibli Automation Future Project Phase 1");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a");
    }

    //method screenshot
    private void takeScreenshot() {
        TakesScreenshot ts = ((TakesScreenshot) webDriver);
        File source = ts.getScreenshotAs(OutputType.FILE);
        screenshotPath = rootPath + "/Screenshots/" + countScreenshot + ".png";
        try {
            FileUtils.copyFile(source, new File(screenshotPath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ++countScreenshot;
    }

    private void assertUrlBlibli(ExtentTest test) throws IOException {
        String url = webDriver.getCurrentUrl();
        takeScreenshot();
        if (url.equals(websiteBlibli)) {
            test.pass("Berhasil membuka website Blibli.com"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil membuka website Blibli.com"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }
    private void assertLihatSemua(ExtentTest test) throws IOException {
        String xpathLihatSemua = null;
        try {
            xpathLihatSemua = String.valueOf(webDriver
                    .findElement(By
                            .xpath("//div[@class='favourite-modal__description'][contains(.,'Pilih 8 kategori kesukaanmu yang akan ditampil di homepage.')]")).getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
        takeScreenshot();
        if (xpathLihatSemua != null) {
            test.pass("Berhasil membuka halaman favorit"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil membuka halaman favorit"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }
    private void assertTravel(ExtentTest test) throws IOException {
        String xpathTravel = null;
        String travel = "Travel";
        try {
            xpathTravel = String.valueOf(webDriver
                    .findElement(By.xpath("//a[contains(@class,'b-active')]")).getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
        takeScreenshot();
        if (xpathTravel != null && xpathTravel.contains(travel)) {
            test.pass("Berhasil membuka tab travel"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil membuka tab travel"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }
    private void assertHotelPage(ExtentTest test) throws IOException {
        String xpathTitleCariHotel = null;
        try {
            xpathTitleCariHotel = String.valueOf(webDriver
                    .findElement(By.xpath("//div[@class='hotel__search-form-title']")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        takeScreenshot();
        if (xpathTitleCariHotel != null) {
            test.pass("Berhasil membuka halaman hotel"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil membuka halaman hotel"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertOpenedSearchPage(ExtentTest test) throws IOException {
        String hasilPencarian = null;
        try {
            hasilPencarian = String.valueOf(webDriver.findElement(By.cssSelector(".sort-title")).getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String urutkanHasil = "Urutkan Hasil";
        takeScreenshot();
        if (hasilPencarian != null && hasilPencarian.contains(urutkanHasil)) {
            test.pass("Berhasil membuka halaman hasil pencarian", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil membuka halaman hasil pencarian", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertJumlahOrang(ExtentTest test) throws IOException {
        webDriver.findElement(By.xpath("//div[contains(text(),'" + jumlahOrang + " orang - 1 kamar')]")).click();
        int xpathJumlahOrang = Integer.parseInt(webDriver
                .findElement(By.xpath("//div[@class='number-picker-section']//div[1]//div[1]//div[2]//div[1]//div[2]//div[1]"))
                .getText());
        takeScreenshot();
        webDriver.findElement(By.xpath("//div[@class='homepage-drop-down active']")).click();
        if (xpathJumlahOrang == jumlahOrang) {
            test.pass("Berhasil menambah jumlah Tamu"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak menambah jumlah Tamu"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertJumlahKamar(ExtentTest test) throws IOException {
        webDriver.findElement(By.xpath("//div[contains(text(),'" + jumlahKamar + " orang - " + jumlahKamar + " kamar')]")).click();
        int xpathJumlahKamar = Integer.parseInt(webDriver
                .findElement(By.xpath("/html/body/div[2]/div/main/div[3]/div/section/div/div[1]/div/div[3]/div[2]/div[4]/div/div/div/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div"))
                .getText());
        takeScreenshot();
        webDriver.findElement(By.xpath("//div[@class='homepage-drop-down active']")).click();
        if (xpathJumlahKamar == jumlahKamar) {
            test.pass("Berhasil menambah jumlah Kamar"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak menambah jumlah Kamar"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertJumlahOrangdanKamarSama(ExtentTest test) throws IOException {
        webDriver.findElement(By.xpath("//div[contains(text(),'" + jumlahOrangdanKamarSama + " orang - " + jumlahOrangdanKamarSama + " kamar')]")).click();
        int xpathJumlahKamar = Integer.parseInt(webDriver
                .findElement(By.xpath("/html/body/div[2]/div/main/div[3]/div/section/div/div[1]/div/div[3]/div[2]/div[4]/div/div/div/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div"))
                .getText());
        int xpathJumlahOrang = Integer.parseInt(webDriver
                .findElement(By.xpath("//div[@class='number-picker-section']//div[1]//div[1]//div[2]//div[1]//div[2]//div[1]"))
                .getText());
        takeScreenshot();
        webDriver.findElement(By.xpath("//div[@class='homepage-drop-down active']")).click();
        if (xpathJumlahKamar == jumlahOrangdanKamarSama && xpathJumlahOrang == jumlahOrangdanKamarSama) {
            test.pass("Berhasil menambah jumlah Kamar dan Tamu"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else if (xpathJumlahKamar == jumlahOrangdanKamarSama) {
            test.pass("Berhasil menambah jumlah Kamar"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
            test.fail("Tidak berhasil menambah jumlah tamu"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else if (xpathJumlahOrang == jumlahOrangdanKamarSama) {
            test.pass("Berhasil menambah jumlah tamu"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
            test.fail("Tidak berhasil menambah jumlah kamar"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil menambah jumlah tamu dan kamar"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertKota(ExtentTest test) throws IOException, InterruptedException {
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("(//div[@class='destination__name'])[1]")));
        webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        Thread.sleep(1000);
        String xpathKota = String.valueOf(webDriver.findElement(By.xpath("(//div[@class='destination__name'])[1]")).getText());
        takeScreenshot();
        webDriver.findElement(By.xpath("(//div[contains(@class,'name')])[1]"))
                .click();
        if (xpathKota.contains(namaKotaHotelBenar)) {
            test.pass("Berhasil memilih kota", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil memilih kota", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertOpenedSearchPageCity(ExtentTest test) throws IOException {
        String xpathKota = String.valueOf(webDriver.findElement(By.xpath("//div[contains(@class,'search-location')]")).getText());
        takeScreenshot();
        if (xpathKota.contains(namaKotaHotelBenar.toLowerCase())) {
            test.pass("Berhasil mencari kota yang dipilih", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil mencari kota yang dipilih", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertJumlahOrangdanKamarBeda(ExtentTest test) throws IOException {
        webDriver.findElement(By.xpath("//div[contains(text(),'" + jumlahOrang + " orang - " + jumlahKamar + " kamar')]")).click();
        int xpathJumlahKamar = Integer.parseInt(webDriver
                .findElement(By.xpath("/html/body/div[2]/div/main/div[3]/div/section/div/div[1]/div/div[3]/div[2]/div[4]/div/div/div/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div"))
                .getText());
        int xpathJumlahOrang = Integer.parseInt(webDriver
                .findElement(By.xpath("//div[@class='number-picker-section']//div[1]//div[1]//div[2]//div[1]//div[2]//div[1]"))
                .getText());
        takeScreenshot();
        webDriver.findElement(By.xpath("//div[@class='homepage-drop-down active']")).click();
        if (xpathJumlahKamar == jumlahKamar && xpathJumlahOrang == jumlahOrang) {
            test.pass("Berhasil menambah jumlah Kamar dan Tamu"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else if (xpathJumlahKamar == jumlahKamar) {
            test.pass("Berhasil menambah jumlah Kamar"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
            test.fail("Tidak berhasil menambah jumlah tamu"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else if (xpathJumlahOrang == jumlahOrang) {
            test.pass("Berhasil menambah jumlah tamu"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
            test.fail("Tidak berhasil menambah jumlah kamar"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil menambah jumlah tamu dan kamar"
                    , MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertOpenedSelectedHotel(ExtentTest test) throws IOException {
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//div[@class='title'][contains(.,'Silakan Pilih Kamar')]")));
        String selectedHotel = String.valueOf(webDriver.findElement(By.xpath("//div[@class='title'][contains(.,'Silakan Pilih Kamar')]")).getText());
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        takeScreenshot();
        if (selectedHotel != null) {
            test.pass("Berhasil memilih hotel yang diinginkan", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil memilih hotel yang diinginkan", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertOpenedSelectedRoom(ExtentTest test) throws IOException {
        webDriver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        String selectedRoom = String
                .valueOf(webDriver
                        .findElement(By.xpath("//div[contains(@class,'guest-form-title font-18 margin-bottom-15 font-bold')]"))
                        .getText());
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        takeScreenshot();
        if (selectedRoom != null) {
            test.pass("Berhasil memilih kamar pada hotel yang diinginkan", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil memilih kamar pada hotel yang diinginkan", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertFilledDataPemesan(ExtentTest test) throws IOException {
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        String xpathNamaPemesan = String.valueOf(
                webDriver.findElement(By.xpath("//input[contains(@placeholder,'Isi Nama Lengkap Pemesan')]")).getAttribute("value"));
        String xpathNomorTelepon = String.valueOf(
                webDriver.findElement(By.xpath("//input[contains(@placeholder,'Isi Nomor Telepon')]")).getAttribute("value"));
        String xpathEmail = String.valueOf(
                webDriver.findElement(By.xpath("//input[contains(@placeholder,'Isi Email')]")).getAttribute("value"));
        String xpathNamaTamu = String.valueOf(
                webDriver.findElement(By.xpath("//input[contains(@placeholder,'Isi Nama Lengkap Tamu')]")).getAttribute("value"));
        takeScreenshot();
        if (xpathNamaPemesan.equals(namaPemesan)) {
            test.pass("Berhasil mengisi nama pemesan", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil mengisi nama pemesan", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
        if (xpathNomorTelepon.equals(nomorTelepon)) {
            test.pass("Berhasil mengisi nomor telepon", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil mengisi nomor telepon", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
        if (xpathEmail.equals(email)) {
            test.pass("Berhasil mengisi email", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil mengisi email", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
        if (xpathNamaTamu.equals(namaTamu)) {
            test.pass("Berhasil mengisi nama tamu", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil mengisi nama tamu", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertHargaTermurah(ExtentTest test) throws IOException {
        int hargaPertama = 0, hargaKedua = 0, hargaKetiga = 0;
        int attempts = 0;
        wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.and(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[1]")),
                ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[2]")),
                ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[3]"))));
        while (attempts < 2) {
            try {
                String stringHargaPertama = String.valueOf(webDriver.findElement(By.xpath("(//div[@class='price'])[1]")).getText())
                        .replaceAll("[^0-9]", "");
                hargaPertama = Integer.parseInt(stringHargaPertama);
                String stringHargaKedua = String.valueOf(webDriver.findElement(By.xpath("(//div[@class='price'])[2]")).getText())
                        .replaceAll("[^0-9]", "");
                hargaKedua = Integer.parseInt(stringHargaKedua);
                String stringHargaKetiga = String.valueOf(webDriver.findElement(By.xpath("(//div[@class='price'])[3]")).getText())
                        .replaceAll("[^0-9]", "");
                hargaKetiga = Integer.parseInt(stringHargaKetiga);
                break;
            } catch (Exception ignored) {
            }
            attempts++;
        }
        takeScreenshot();
        if (hargaPertama <= hargaKedua && hargaKedua <= hargaKetiga) {
            test.pass("Berhasil mengurutkan harga termurah", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil mengurutkan harga termurah", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertHargaTermahal(ExtentTest test) throws IOException {
        int hargaPertama = 0, hargaKedua = 0, hargaKetiga = 0;
        int attempts = 0;
        wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.and(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[1]")),
                ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[2]")),
                ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[3]"))));
        while (attempts < 2) {
            try {
                String stringHargaPertama = String.valueOf(webDriver.findElement(By.xpath("(//div[@class='price'])[1]")).getText())
                        .replaceAll("[^0-9]", "");
                hargaPertama = Integer.parseInt(stringHargaPertama);
                String stringHargaKedua = String.valueOf(webDriver.findElement(By.xpath("(//div[@class='price'])[2]")).getText())
                        .replaceAll("[^0-9]", "");
                hargaKedua = Integer.parseInt(stringHargaKedua);
                String stringHargaKetiga = String.valueOf(webDriver.findElement(By.xpath("(//div[@class='price'])[3]")).getText())
                        .replaceAll("[^0-9]", "");
                hargaKetiga = Integer.parseInt(stringHargaKetiga);
                break;
            } catch (Exception ignored) {
            }
            attempts++;
        }
        takeScreenshot();
        if (hargaPertama >= hargaKedua && hargaKedua >= hargaKetiga) {
            test.pass("Berhasil mengurutkan harga termahal", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil mengurutkan harga termahal", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }

    private void assertReviewTerbaik(ExtentTest test) throws IOException {
        int ratingPertama = 0, ratingKedua = 0, ratingKetiga = 0;
        int attempts = 0;
        wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.and(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[1]")),
                ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[2]")),
                ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[3]"))));
        while (attempts < 2) {
            try {
                String stringRatingPertama = String.valueOf(webDriver
                        .findElement(By
                                .cssSelector(".right-content > span:nth-child(2) > div:nth-child(2) > div:nth-child(1) > a:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1)"))
                        .getText())
                        .replaceAll("[^0-9]", "");
                ratingPertama = Integer.parseInt(stringRatingPertama);
                String stringRatingKedua = String.valueOf(webDriver
                        .findElement(By
                                .cssSelector(".right-content > span:nth-child(2) > div:nth-child(2) > div:nth-child(2) > a:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1)"))
                        .getText())
                        .replaceAll("[^0-9]", "");
                ratingKedua = Integer.parseInt(stringRatingKedua);
                String stringRatingKetiga = String.valueOf(webDriver
                        .findElement(By
                                .cssSelector(".right-content > span:nth-child(2) > div:nth-child(2) > div:nth-child(3) > a:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1)"))
                        .getText())
                        .replaceAll("[^0-9]", "");
                ratingKetiga = Integer.parseInt(stringRatingKetiga);
                break;
            } catch (Exception ignored) {
            }
            attempts++;
        }
        takeScreenshot();
        if (ratingPertama >= ratingKedua && ratingKedua >= ratingKetiga) {
            test.pass("Berhasil mengurutkan review terbaik", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        } else {
            test.fail("Tidak berhasil mengurutkan review terbaik", MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
        }
    }
    @Before
    public void initiate() throws IOException {
        appProps.load(new FileInputStream(appConfigPath));

        //inisialisasi variabel dengan property
        String geckoDriverPath = appProps.getProperty("geckoDriverPath");
        namaKotaHotelBenar = appProps.getProperty("namaKotaHotelBenar");
        jumlahOrang = Integer.parseInt(appProps.getProperty("jumlahOrang"));
        jumlahKamar = Integer.parseInt(appProps.getProperty("jumlahKamar"));
        jumlahOrangdanKamarSama = Integer.parseInt(appProps.getProperty("jumlahOrangdanKamarSama"));
        websiteBlibli = appProps.getProperty("websiteBlibli");
        namaPemesan = appProps.getProperty("namaPemesan");
        nomorTelepon = appProps.getProperty("nomorTelepon");
        email = appProps.getProperty("email");
        namaTamu = appProps.getProperty("namaTamu");

        //inisialisasi webdriver
        System.setProperty("webdriver.gecko.driver", geckoDriverPath);

        //browsernya
        switch (browser) {
            case "chrome":
                ChromeOptions options = new ChromeOptions();
                options.addArguments("start-maximized");
                webDriver = new ChromeDriver(options);
                break;
            case "firefox":
                webDriver = new FirefoxDriver();
                webDriver.manage().window().maximize();
                break;
        }
    }

    @After
    public void tearDown() {
        try {
            webDriver.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        extent.flush();
    }

    /**
     * Rigorous Test :-)
     */
    @Test
    public void bukaHotelPage() throws IOException {
        // creating tests
        ExtentTest test = extent.createTest("Default dan tombol hotel"
                , "Membuka hasil pencarian hotel dengan default dan tombol hotel");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        webDriver.get(websiteBlibli);
//        homepage.closeAlertNotification();
//        homepage.closePromo();
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
    }
    @Test
    public void bukaHotelPageLihatSemua() throws IOException {
        // creating tests
        ExtentTest test = extent.createTest("Membuka hotel page dengan lihat semua"
                , "Membuka hotel page dengan lihat semua kemudian memilih travel dan hotel, kemudian menampilkan hasilnya");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        favouritesPage favouritesPage = new favouritesPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickLihatSemua();
        assertLihatSemua(test);
        favouritesPage.clickTravel();
        assertTravel(test);
        favouritesPage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
    }
    @Test
    public void mencariKotaHotelAda() throws IOException, InterruptedException {
        // creating tests
        ExtentTest test = extent.createTest("Mencari kota", "Membuka hasil pencarian hotel dengan kota yang tersedia");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.searchCity(namaKotaHotelBenar);
        assertKota(test);           //assert kota sudah terpilih
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPageCity(test);       //assert result page opened
    }

    @Test
    public void InputJumlahOrang() throws IOException {
        // creating tests
        ExtentTest test = extent.createTest("Menginput jumlah orang", "Menginput jumlah tamu yang akan menginap yang dipesan pada halaman hotel");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.inputJumlahOrang(jumlahOrang);
        assertJumlahOrang(test);
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
    }

    @Test
    public void InputJumlahKamar() throws IOException {
        // creating tests
        ExtentTest test = extent.createTest("Menginput jumlah kamar", "Menginput jumlah kamar yang dipesan pada halaman hotel");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.inputJumlahKamar(jumlahKamar);
        assertJumlahKamar(test);
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
    }

    @Test
    public void InputJumlahOrangdanKamarSama() throws IOException {
        // creating tests
        ExtentTest test = extent.createTest("Menginput jumlah kamar dan tamu sama"
                , "Menginput jumlah kamar yang dipesan dan tamu yang akan menginap sama jumlahnya dengan kamar yang dipesan pada halaman hotel");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.inputJumlahOrangdanKamarSama(jumlahOrangdanKamarSama);
        assertJumlahOrangdanKamarSama(test);    //assert penambahan jumlah kamar dan tamu
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
    }

    @Test
    public void InputJumlahOrangdanKamarBeda() throws IOException {
        // creating tests
        ExtentTest test = extent.createTest("Menginput jumlah kamar dan tamu beda"
                , "Menginput jumlah kamar yang dipesan dan tamu yang akan menginap berbeda jumlahnya dengan kamar yang dipesan pada halaman hotel");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.inputJumlahOrangdanKamarBeda(jumlahOrang, jumlahKamar);
        assertJumlahOrangdanKamarBeda(test);    //assert penambahan jumlah kamar dan tamu
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
    }

    @Test
    public void memilihHotel() throws IOException {
        // creating tests
        ExtentTest test = extent.createTest("Memilih hotel"
                , "Memilih hotel teratas dari hasil pencarian");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        hasilSearchPage hasilSearchPage = new hasilSearchPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
        hasilSearchPage.pilihHotel();
        assertOpenedSelectedHotel(test);
    }

    @Test
    public void memilihKamarHotel() throws IOException {
        // creating tests
        ExtentTest test = extent.createTest("Memilih kamar hotel"
                , "Memilih kamar hotel teratas dari hotel yang sudah dipilih");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        hasilSearchPage hasilSearchPage = new hasilSearchPage(webDriver);
        hotelDipilihPage hotelDipilihPage = new hotelDipilihPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
        hasilSearchPage.pilihHotel();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        assertOpenedSelectedHotel(test);
        hotelDipilihPage.pilihKamar();
        assertOpenedSelectedRoom(test);
    }

    @Test
    public void mengisiDataPemesan() throws IOException, InterruptedException {
        // creating tests
        ExtentTest test = extent.createTest("Mengisi data pemesan"
                , "Mengisi data pemesan pada kamar hotel yang dipilih");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        hasilSearchPage hasilSearchPage = new hasilSearchPage(webDriver);
        hotelDipilihPage hotelDipilihPage = new hotelDipilihPage(webDriver);
        dataPemesanPage dataPemesanPage = new dataPemesanPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.searchCity(namaKotaHotelBenar);
        assertKota(test);           //assert kota sudah terpilih
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
        hasilSearchPage.pilihHotel();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        assertOpenedSelectedHotel(test);
        hotelDipilihPage.pilihKamar();
        assertOpenedSelectedRoom(test);
        dataPemesanPage.MengisiDataPemesan(namaPemesan, nomorTelepon, email, namaTamu);
        assertFilledDataPemesan(test);
    }

    @Test
    public void urutkanHargaTermurah() throws IOException, InterruptedException {
        // creating tests
        ExtentTest test = extent.createTest("Mengurutkan Harga Termurah"
                , "Membuka hasil pencarian hotel kemudian mengurutkan harga termurah");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        hasilSearchPage hasilSearchPage = new hasilSearchPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
        hasilSearchPage.urutkanHargaTermurah();
        Thread.sleep(2000);
        assertHargaTermurah(test);
    }

    @Test
    public void urutkanHargaTermahal() throws IOException, InterruptedException {
        // creating tests
        ExtentTest test = extent.createTest("Mengurutkan Harga Termahal"
                , "Membuka hasil pencarian hotel kemudian mengurutkan harga termahal");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        hasilSearchPage hasilSearchPage = new hasilSearchPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
        hasilSearchPage.urutkanHargaTermahal();
        Thread.sleep(2000);
        assertHargaTermahal(test);
    }

    @Test
    public void urutkanReviewTerbaik() throws IOException, InterruptedException {
        // creating tests
        ExtentTest test = extent.createTest("Mengurutkan review terbaik"
                , "Membuka hasil pencarian hotel kemudian mengurutkan review terbaik");

        homepage homepage = new homepage(webDriver);
        hotelPage hotelPage = new hotelPage(webDriver);
        hasilSearchPage hasilSearchPage = new hasilSearchPage(webDriver);
        webDriver.get(websiteBlibli);
        //explicit wait
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")));
        assertUrlBlibli(test);      //assert website blibli opened
        homepage.clickHotel();
        assertHotelPage(test);      //assert hotel page opened
        hotelPage.cariHotel();
        wait = new WebDriverWait(webDriver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".sort-title")));
        assertOpenedSearchPage(test);       //assert result page opened
        hasilSearchPage.urutkanReviewTerbaik();
        Thread.sleep(2000);
        assertReviewTerbaik(test);
    }
}