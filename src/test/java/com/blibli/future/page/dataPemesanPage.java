package com.blibli.future.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class dataPemesanPage {
    private WebDriver webDriver;
    private WebDriverWait wait;

    public dataPemesanPage(WebDriver driver) {
        this.webDriver = driver;
    }

    public void MengisiDataPemesan(String nama, String nomerTelfon, String email, String namaTamu) {
        webDriver.navigate().refresh();
        wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//input[contains(@placeholder,'Isi Nama Lengkap Pemesan')]")));
        webDriver.findElement(By.xpath("//input[contains(@placeholder,'Isi Nama Lengkap Pemesan')]")).sendKeys(nama);
        webDriver.findElement(By.xpath("//input[contains(@placeholder,'Isi Nomor Telepon')]")).sendKeys(nomerTelfon);
        webDriver.findElement(By.xpath("//input[contains(@placeholder,'Isi Email')]")).sendKeys(email);
        if (nama.equals(namaTamu)) {
            webDriver.findElement(By.xpath("//input[contains(@class,'travel-checkbox margin-left-15')]")).click();
        } else {
            webDriver.findElement(By.xpath("//input[contains(@placeholder,'Isi Nama Lengkap Tamu')]")).sendKeys(namaTamu);
        }
    }
}
