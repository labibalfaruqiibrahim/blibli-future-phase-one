package com.blibli.future.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class favouritesPage {

    private WebDriver webDriver;

    public favouritesPage(WebDriver driver) {
        this.webDriver = driver;
    }

    public void clickTravel() {
        webDriver.findElement(By.xpath("//a[contains(.,'Travel')]")).click();
    }

    public void clickHotel() {
        webDriver.findElement(By.xpath("//div[@class='favourite-modal__content']//div[3]//a[1]//div[1]//img[1]")).click();
    }
}
