package com.blibli.future.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class hasilSearchPage {

    private WebDriver webDriver;

    public hasilSearchPage(WebDriver driver) {
        this.webDriver = driver;
    }
    public void pilihHotel(){
        webDriver.findElement(By.
                cssSelector(".right-content > span:nth-child(2) > div:nth-child(2) > div:nth-child(1) > a:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)"))
                .click();
    }
    public void urutkanHargaTermurah() {
        webDriver.findElement(By.xpath("//div[@class='sort-box'][contains(.,'Harga Termurah')]")).click();
    }
    public void urutkanHargaTermahal() {
        webDriver.findElement(By.xpath("//div[@class='sort-box'][contains(.,'Harga Termahal')]")).click();
    }
    public void urutkanReviewTerbaik() {
        webDriver.findElement(By.xpath("//div[@class='sort-box'][contains(.,'Review Terbaik')]")).click();
    }
}