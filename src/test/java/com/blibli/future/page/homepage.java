package com.blibli.future.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class homepage {
    private WebDriver webDriver;
    public homepage(WebDriver driver) {
        this.webDriver = driver;
    }
    public void closeAlertNotification() {
        try {
            webDriver.findElement(By.xpath("//*[@id=\"desktopBannerWrapped\"]/div/div[3]/div[1]/button[1]")).click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void closePromo() {
        try {
            webDriver.findElement(By.xpath("//*[@id=\"main-app\"]/div[4]/div/div/div[1]/i")).click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void clickHotel() {
        webDriver.findElement(By.xpath("//section[@class='b-widescreen favourite-other']//div[6]//img[1]")).click();
    }
    public void clickLihatSemua(){
        webDriver.findElement(By.xpath("//a[@href='/home/favourites'][contains(.,'Lihat Semua')]")).click();
    }
}
