package com.blibli.future.page;

import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;

public class hotelDipilihPage {
    private WebDriver webDriver;

    public hotelDipilihPage(WebDriver driver) {
        this.webDriver = driver;
    }

    public void pilihKamar() {
        String xpathPilihKamar = null;
        int i=0;
        do {
            webDriver.navigate().refresh();
            try{
            webDriver.findElement(By.
                    xpath("(//button[@class='room-box-third-section-button'][contains(.,'Pesan Kamar Ini')])[1]"))
                    .click();
            webDriver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);

                xpathPilihKamar = String.valueOf(webDriver
                        .findElement(By.xpath("//div[contains(@class,'guest-form-title font-18 margin-bottom-15 font-bold')]")).getText());
            }catch (Exception ignored){}
            i++;
        } while (xpathPilihKamar == null && i < 3);
    }
}
