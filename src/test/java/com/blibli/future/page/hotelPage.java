package com.blibli.future.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.Keys;


public class hotelPage {
    private WebDriver webDriver;

    public hotelPage(WebDriver driver) {
        this.webDriver = driver;
    }
    public void searchCity(String namaKotaHotelBenar) {
        String selectAll = Keys.chord(Keys.CONTROL, "a");
        webDriver.findElement(By.xpath("//input[@placeholder='Kota atau nama hotel']"))
                .sendKeys(selectAll);
        webDriver.findElement(By.xpath("//input[@placeholder='Kota atau nama hotel']"))
                .sendKeys(namaKotaHotelBenar);
    }
    public void inputJumlahOrang(int jumlahOrang) {
        webDriver.findElement(By.xpath("//div[contains(text(),'1 orang - 1 kamar')]")).click();
        for (int i = 1; i < jumlahOrang; i++) {
            webDriver.findElement(By.xpath("//div[@class='number-picker-section']//div[1]//div[1]//div[2]//div[1]//div[3]//button[1]")).click();
        }
        webDriver.findElement(By.xpath("//div[@class='homepage-drop-down active']")).click();
    }
    public void inputJumlahKamar(int jumlahKamar) {
        webDriver.findElement(By.xpath("//div[contains(text(),'1 orang - 1 kamar')]")).click();
        for (int i = 1; i < jumlahKamar; i++) {
            webDriver.findElement(By.xpath("//body//div[@class='homepage-drop-down active']//div//div//div[2]//div[1]//div[2]//div[1]//div[3]//button[1]")).click();
        }
        webDriver.findElement(By.xpath("//div[@class='homepage-drop-down active']")).click();
    }
    public void inputJumlahOrangdanKamarSama(int jumlahOrangdanKamarSama) {
        webDriver.findElement(By.xpath("//div[contains(text(),'1 orang - 1 kamar')]")).click();
        for (int i = 1; i < jumlahOrangdanKamarSama; i++) {
            webDriver.findElement(By.xpath("//div[@class='number-picker-section']//div[1]//div[1]//div[2]//div[1]//div[3]//button[1]")).click();
            webDriver.findElement(By.xpath("//body//div[@class='homepage-drop-down active']//div//div//div[2]//div[1]//div[2]//div[1]//div[3]//button[1]")).click();
        }
        webDriver.findElement(By.xpath("//div[@class='homepage-drop-down active']")).click();
    }
    public void inputJumlahOrangdanKamarBeda(int jumlahOrang, int jumlahKamar) {
        webDriver.findElement(By.xpath("//div[contains(text(),'1 orang - 1 kamar')]")).click();
        for (int i = 1; i < jumlahOrang; i++) {
            webDriver.findElement(By.xpath("//div[@class='number-picker-section']//div[1]//div[1]//div[2]//div[1]//div[3]//button[1]")).click();
        }
        for (int i = 1; i < jumlahKamar; i++) {
            webDriver.findElement(By.xpath("//body//div[@class='homepage-drop-down active']//div//div//div[2]//div[1]//div[2]//div[1]//div[3]//button[1]")).click();
        }
        webDriver.findElement(By.xpath("//div[@class='homepage-drop-down active']")).click();
    }
    public void cariHotel() {
        try {
            webDriver.findElement(By.cssSelector(".hotel__search-form-title")).click();
        } catch (Exception e) {
            e.printStackTrace();
        }
        webDriver.findElement(By.xpath("//button[contains(.,'Cari Hotel')]")).click();
    }
}
